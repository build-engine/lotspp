(local CONF {:_AUTHOR "parlortricks"
             :_EMAIL "palortricks@fastmail.fm"
             :_NAME "LOTSPP"
             :_VERSION "1.0"
             :_URL "https://gitlab.com/build-engine/lotspp"
             :_DESCRIPTION "Legend of the Seven Paladins Packer"
             :_SPDX "MIT License"
             :_SPDXURL "https://spdx.org/licenses/MIT"
             :_LICENSE "    MIT LICENSE
    Copyright 2022 parlortricks <parlortricks@fastmail.fm>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the \"Software\"), to 
    deal in the Software without restriction, including without limitation the 
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or 
    sell copies of the Software, and to permit persons to whom the Software is 
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in 
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE."})

(set CONF._HELP (..
"=====================================================\n"
CONF._DESCRIPTION " v" CONF._VERSION "\n" 
"=====================================================\n"
CONF._SPDX " -> " CONF._SPDXURL "\n"
"Copyright 2022 " CONF._AUTHOR " <" CONF._EMAIL ">\n\n"
CONF._URL "\n"
"=====================================================\n\n"
"Usage: fennel lotspp.fnl [file1] [file2] [file3] ...
  
  Currently outputs ARCHIVE.DAT, rename to your liking.

  Options:
  -h, --help      Display this help
  -v, --version   Display the version  
   
Based on my work creating LOTSPE in Fennel"))

(fn file-exists [name]
  (let [f (io.open name :r)]
    (if (not= f nil)
      (do
        (io.close f)
        true)
      false)))

(fn file-list [arg]
  (let [files []]
    (each [_ file (ipairs arg)]
      (if (file-exists file)
        (table.insert files file)))
    files))

(fn process-files [data]
  (let [files []]
    (each [_ arg (ipairs data)]
      (let [file {}]
        (with-open [fin (io.open arg :rb)]          
          (set file.data (fin:read "*all"))
          (set file.size (fin:seek :end)))
        (table.insert files file)))
    files))

(fn pack-files [data]
  (var summed-files 0)
  (each [_ file (ipairs data)]
    (set summed-files (+ summed-files file.size)))

  (let [header-size (+ (* (length data) 4) 4)
        archive-size (+ header-size summed-files)]
    (var header-offset 0)
    (var file-offset header-size)
    (with-open [fout (io.open "ARCHIVE.DAT" :wb)]
      (each [_ file (ipairs data)]
        (fout:seek :set header-offset)
        (fout:write (string.pack "<I4" file-offset))
        (set header-offset (fout:seek))
        (fout:seek :set file-offset)
        (fout:write file.data)
        (set file-offset (fout:seek)))
      (fout:seek :set header-offset)
      (fout:write (string.pack "<I4" archive-size)))))

(if (or (= (length arg) 0) (: (. arg 1) :match "-h") (: (. arg 1) :match "--help"))
      (print CONF._HELP)
    (or (: (. arg 1) :match "-v") (: (. arg 1) :match "--version"))
      (print (.. CONF._NAME " v" CONF._VERSION))
    (pack-files (process-files (file-list arg))))

